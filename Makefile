prepare:
	@echo add wasm target

start_frontend:
	@cd frontend; trunk serve
start_backend:
	@cargo run

build:
	@cd frontend; trunk build --release
	@cargo run --release
