/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/**/*.{html,rs}",
        "./index.html",
    ],
    theme: {
        extend: {},
    },
    darkMode: "class",
    plugins: [],
}

