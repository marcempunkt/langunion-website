use yew::{Html, UseStateHandle, Callback, html, function_component, use_state};

#[function_component]
pub fn Home() -> Html {
    let clicked: UseStateHandle<bool> = use_state(|| false);

    let onclick = {
        let clicked = clicked.clone();
        Callback::from(move |_| {
            clicked.set(!*clicked);
        })
    };

    html! {
        <div class="bg-slate-800 text-white w-full h-full">
            <h1>{"Hi from frontend!"}</h1>
            <p>{format!("clicked? {}", *clicked)}</p>
            <button {onclick}>{"click me!"}</button>
        </div>
    }
}
