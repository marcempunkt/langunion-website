use yew::{Properties, Html, html, function_component};
use yew_router::{Router, BrowserRouter, Routable, Switch};
use yew_router::history::{History, AnyHistory, MemoryHistory};
use crate::pages::Home;

mod pages;

#[derive(Routable, PartialEq, Clone)]
enum Route {
    #[at("/")]
    Home,
    #[not_found]
    #[at("/404")]
    NotFound,
}

#[function_component]
pub fn App() -> Html {
    html! {
        <BrowserRouter>
            <Switch<Route> render={switch} />
        </BrowserRouter>
    }
}

#[derive(Properties, PartialEq)]
pub struct ServerAppProps {
    pub uri: String,
}

#[function_component]
pub fn ServerApp(props: &ServerAppProps) -> Html {
    let history = AnyHistory::from(MemoryHistory::new());

    history.push(&props.uri);

    html! {
        <Router history={history}>
            <Switch<Route> render={switch} />
        </Router>
    }
}

fn switch(route: Route) -> Html {
    match route {
        Route::Home => html!{
            <Home />
        },
        Route::NotFound => html! {
            <h1>{"Not found"}</h1>
        },
    }
}
