use actix_cors::Cors;
use actix_files::Files;
use actix_web::{
    get, middleware::Logger, App as BackendApp, HttpRequest, HttpResponse, HttpServer,
};
use frontend::{ServerApp, ServerAppProps};
use std::path::Path;
use tokio::fs;

static FRONTEND_DIST_DIR: &str = "./dist";

#[actix_web::main]
async fn main() {
    env_logger::Builder::from_env(env_logger::Env::default().filter_or("LOG_LEVEL", "info"))
        .init();

    HttpServer::new(move || {
        BackendApp::new()
            .wrap(Cors::permissive())
            .wrap(Logger::default())
            .service(Files::new("/dist", FRONTEND_DIST_DIR))
            .service(frontend_app)
    })
    .bind(("0.0.0.0", 3100))
    .unwrap()
    .run()
    .await
    .unwrap();
}

#[get("/{tail:.*}")]
async fn frontend_app(req: HttpRequest) -> HttpResponse {
    let props = ServerAppProps {
        uri: req.uri().to_string(),
    };

    let frontend_app: String =
        yew::ServerRenderer::<ServerApp>::with_props(|| props)
        .render()
        .await;

    let html: String = fs::read_to_string(Path::new(FRONTEND_DIST_DIR).join("index.html"))
        .await
        .unwrap()
        .replace("<body>", &format!("<body>{frontend_app}"));

    HttpResponse::Ok().body(html)
}
